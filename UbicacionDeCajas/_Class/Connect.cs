﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace UbicacionDeCajas.Class
{
    class Connect
    {
        private string _id;
        private bool _check;
        private string _caja;
        private string _cliente;
        private int _viaje;
        private string _almacen;
        private string _fecha;
        private string _buscar;

        public string Id { get => _id; set => _id = value; }
        public bool Check { get => _check; set => _check = value; }
        public string Caja { get => _caja; set => _caja = value; }
        public string Cliente { get => _cliente; set => _cliente = value; }
        public int Viaje { get => _viaje; set => _viaje = value; }
        public string Almacen { get => _almacen; set => _almacen = value; }
        public string Fecha { get => _fecha; set => _fecha = value; }
        public string Buscar { get => _buscar; set => _buscar = value; }

        //public void Conection()
        //{
        //    //Crearemos la cadena de conexión concatenando las variables
        //    string cadenaConexion = "Database=" + bd + "; Data Source=" + servidor + "; User Id=" + usuario + "; Password=" + password + ";";

        //    //Instancia para conexión a MySQL, recibe la cadena de conexión
        //    MySqlConnection conexionBD = new MySqlConnection(cadenaConexion);
        //    MySqlDataReader reader = null; //Variable para leer el resultado de la consulta

        //    //Agregamos try-catch para capturar posibles errores de conexión o sintaxis.
        //    try
        //    {
        //        string consulta = "SHOW DATABASES"; //Consulta a MySQL (Muestra las bases de datos que tiene el servidor)
        //        MySqlCommand comando = new MySqlCommand(consulta); //Declaración SQL para ejecutar contra una base de datos MySQL
        //        comando.Connection = conexionBD; //Establece la MySqlConnection utilizada por esta instancia de MySqlCommand
        //        conexionBD.Open(); //Abre la conexión
        //        reader = comando.ExecuteReader(); //Ejecuta la consulta y crea un MySqlDataReader

        //        while (reader.Read()) //Avanza MySqlDataReader al siguiente registro
        //        {
        //            datos += reader.GetString(0) + "\n"; //Almacena cada registro con un salto de linea
        //        }

        //        //MessageBox.Show(datos); //Imprime en cuadro de dialogo el resultado
        //    }
        //    catch (MySqlException ex)
        //    {
        //        //MessageBox.Show(ex.Message); //Si existe un error aquí muestra el mensaje
        //    }
        //    finally
        //    {
        //        conexionBD.Close(); //Cierra la conexión a MySQL
        //    }
        //}

        public static string Cadena(string DB)
        {
            string servidor = "192.168.10.51";
            //string servidor = "192.168.1.111"; //Nombre o ip del servidor de MySQL
            string bd = DB; //Nombre de la base de datos
            string usuario = "hungarosco538669"; //Usuario de acceso a MySQL
            string password = "Hungaros20"; //Contraseña de usuario de acceso a MySQL            

            return "Database=" + bd + "; Data Source=" + servidor + "; User Id=" + usuario + "; Password=" + password + ";";
        }

        private static Tuple<List<Connect>> Busca(string sql, int x)
        {
            List<Connect> listaTrayectos = new List<Connect>();
                        

            //Crearemos la cadena de conexión concatenando las variables
            string cadenaConexion = Cadena("hundatamov_hungarosco538669");

            //Instancia para conexión a MySQL, recibe la cadena de conexión
            MySqlConnection conexionBD = new MySqlConnection(cadenaConexion);
            MySqlDataReader reader = null; //Variable para leer el resultado de la consulta

            try
            {
                MySqlCommand comando = new MySqlCommand(sql); //Declaración SQL para ejecutar contra una base de datos MySQL
                comando.Connection = conexionBD; //Establece la MySqlConnection utilizada por esta instancia de MySqlCommand
                conexionBD.Open(); //Abre la conexión
                reader = comando.ExecuteReader(); //Ejecuta la consulta y crea un MySqlDataReader

                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                while (reader.Read()) //Avanza MySqlDataReader al siguiente registro
                {
                    Connect c = new Connect();
                    c.Id = reader["ID"].ToString();
                    if (x == 0)
                        c.Check = Convert.ToBoolean(reader["CHECKBOX"]);
                    //if (x == 1)
                    c.Cliente = reader["CLIENTE"].ToString();
                    c.Caja = reader["CAJA"].ToString();
                    c.Viaje = Convert.ToInt32(reader["VIAJE"]);
                    c.Almacen = reader["ALMACEN"].ToString();
                    if (string.IsNullOrEmpty(reader["FECHA"]?.ToString()))
                        c.Fecha = "Sin Fecha";
                    else
                        c.Fecha = Convert.ToDateTime(reader["FECHA"]).ToString("MM/dd/yyyy");                  
                    listaTrayectos.Add(c);
                }

                //MessageBox.Show(datos); //Imprime en cuadro de dialogo el resultado
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message); //Si existe un error aquí muestra el mensaje
            }
            finally
            {
                conexionBD.Close(); //Cierra la conexión a MySQL
            }
            return new Tuple<List<Connect>>(listaTrayectos);
        }

        public List<Connect> GetAllCajas()
        {
            string sql = "SELECT * FROM hundatamov_hungarosco538669.caja_ubicacion WHERE checkbox = FALSE AND (viaje LIKE '%" + Buscar + "%' OR caja LIKE '%" + Buscar + "%') ORDER BY fecha DESC";
            Tuple<List<Connect>> listaTrayectos = Busca(sql, 0);
            return listaTrayectos.Item1;
            //return ListCliente.Item2;
        }

        public List<Connect> GetCajasPendientes()
        {
            string sql = "SELECT * FROM hundatamov_hungarosco538669.caja_ubicacion WHERE viaje LIKE '%" + Buscar + "%' OR caja LIKE '%" + Buscar + "%' ORDER BY fecha DESC";
            Tuple<List<Connect>> listaTrayectos = Busca(sql, 0);
            return listaTrayectos.Item1;
            //return ListCliente.Item2;
        }

        public List<Connect> GetAllTrayecto()
        {
            string sql = "SELECT CLIENTE.NOMBRE AS CLIENTE , VIAJE.CLIENTE AS CLIENTE ,VIAJE.NUMERO AS ID,VIAJE.IDCAJA AS CAJA, VIAJE.VIAJENUMERO AS VIAJE,ALMACEN.ALMACEN,VIAJE.FECHATER AS FECHA, VIAJE.NUMTRAJECTO " +
                "FROM hundatamov_hungarosco538669.trajectoviajedet AS VIAJE " +
                "INNER JOIN hundatamov_hungarosco538669.almacenesori as ALMACEN ON VIAJE.ALMACENDESTINO = ALMACEN.IDALMACEN " +
                "INNER JOIN hundatamov_hungarosco538669.clientes as CLIENTE ON VIAJE.CLIENTE = CLIENTE.NUMERO " +
                "WHERE VIAJE.VIAJENUMERO LIKE '%" + Buscar + "%' OR VIAJE.IDCAJA LIKE '%" + Buscar + "%'  " +
                "ORDER BY VIAJE.VIAJENUMERO DESC,VIAJE.NUMTRAJECTO DESC";
            Tuple<List<Connect>> listaTrayectos = Busca(sql, 1);
            return listaTrayectos.Item1;
            //return ListCliente.Item2;
        }

        public List<Connect> GetAllAlmacen()
        {
            string sql = "SELECT VIAJE.NUMERO AS ID,VIAJE.IDCAJA AS CAJA, VIAJE.VIAJENUMERO AS VIAJE,CLIENTE.NOMBRE AS CLIENTE ,ALMACEN.ALMACEN,VIAJE.FECHATER AS FECHA " +
                "FROM hundatamov_hungarosco538669.trajectoviajedet AS VIAJE " +
                "INNER JOIN hundatamov_hungarosco538669.almacenesori as ALMACEN ON VIAJE.ALMACENDESTINO = ALMACEN.IDALMACEN " +
                "INNER JOIN hundatamov_hungarosco538669.clientes as CLIENTE ON VIAJE.CLIENTE = CLIENTE.NUMERO " +
                "WHERE (ALMACEN.ALMACEN LIKE '%" + Buscar + "%' OR VIAJE.IDCAJA LIKE '%" + Buscar + "%') AND  VIAJE.VIAJENUMERO IN (" +
                "SELECT MAX(I.VIAJENUMERO) FROM hundatamov_hungarosco538669.trajectoviajedet AS I WHERE I.IDCAJA <> '' GROUP BY I.IDCAJA ) " +
                "GROUP BY VIAJE.IDCAJA";
            Tuple<List<Connect>> listaTrayectos = Busca(sql, 1);
            return listaTrayectos.Item1;
            //return ListCliente.Item2;
        }

        private int Method(string sql)
        {
            int record = 0;

            //Crearemos la cadena de conexión concatenando las variables
            string cadenaConexion = Cadena("hundatamov_hungarosco538669");

            MySqlConnection conexionBD = new MySqlConnection(cadenaConexion);

            try
            {
                using (conexionBD)
                {
                    conexionBD.Open();
                    MySqlCommand cmd = new MySqlCommand(sql, conexionBD);
                    record = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            finally
            {
                conexionBD.Close();
            }
            return record;
        }

        public int InsertarCaja()
        {
            string sql = "INSERT INTO hundatamov_hungarosco538669.caja_ubicacion (checkbox,caja,viaje,almacen,fecha,cliente) VALUES (" + Check + ",'" + Caja + "','" + Viaje + "','" + Almacen + "','" + Fecha + "','" + Cliente + "')";
            return Method(sql);
        }

        public int UpdateCaja()
        {
            string sql = "UPDATE hundatamov_hungarosco538669.caja_ubicacion SET checkbox = " + Check + " WHERE id = " + Id;
            return Method(sql);
        }

        public int DeleteCaja()
        {
            string sql = "DELETE FROM hundatamov_hungarosco538669.caja_ubicacion WHERE checkbox = true";
            return Method(sql);
        }

        public int Actualizar()
        {
            string sql = " UPDATE hundatamov_hungarosco538669.caja_ubicacion SET checkbox = '1' WHERE ID <> 0 AND ID IN ( SELECT * FROM(SELECT V.id AS ID FROM hundatamov_hungarosco538669.trajectoviajedet AS I " +
                "RIGHT JOIN hundatamov_hungarosco538669.caja_ubicacion AS V ON I.IDCAJA = V.caja WHERE v.checkbox = FALSE AND V.viaje NOT IN (SELECT max(I.VIAJENUMERO) AS N_VIAJE FROM hundatamov_hungarosco538669.trajectoviajedet AS I " +
                "RIGHT  JOIN hundatamov_hungarosco538669.caja_ubicacion AS V ON I.IDCAJA = V.caja WHERE v.checkbox = FALSE GROUP BY I.IDCAJA) GROUP BY I.IDCAJA) AS tblTmp )";
            return Method(sql);
        }
    }
}
