﻿namespace UbicacionDeCajas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tbxBuscar = new System.Windows.Forms.ToolStripTextBox();
            this.btnBuscar = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.anchoAutomaticoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todasLasCajasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.limpiarHistorialToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.Add3 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Caja3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Viaje3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Almacen3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cliente3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buscar3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkBox3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.Add2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Caja2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Viaje2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Almacen2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cliente2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buscar2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkBox2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Caja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Viaje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Almacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buscar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.configuracionesPorDefectoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbxBuscar,
            this.btnBuscar,
            this.toolStripDropDownButton1,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(336, 33);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tbxBuscar
            // 
            this.tbxBuscar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbxBuscar.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tbxBuscar.Margin = new System.Windows.Forms.Padding(8, 5, 1, 5);
            this.tbxBuscar.MaxLength = 100;
            this.tbxBuscar.Name = "tbxBuscar";
            this.tbxBuscar.Size = new System.Drawing.Size(217, 23);
            this.tbxBuscar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxBuscar_KeyPress);
            // 
            // btnBuscar
            // 
            this.btnBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(23, 30);
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.anchoAutomaticoToolStripMenuItem,
            this.todasLasCajasToolStripMenuItem,
            this.limpiarHistorialToolStripMenuItem1,
            this.configuracionesPorDefectoToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(29, 30);
            this.toolStripDropDownButton1.Text = "toolStripDropDownButton1";
            // 
            // anchoAutomaticoToolStripMenuItem
            // 
            this.anchoAutomaticoToolStripMenuItem.Checked = true;
            this.anchoAutomaticoToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.anchoAutomaticoToolStripMenuItem.Name = "anchoAutomaticoToolStripMenuItem";
            this.anchoAutomaticoToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.anchoAutomaticoToolStripMenuItem.Text = "Ancho automatico";
            this.anchoAutomaticoToolStripMenuItem.Click += new System.EventHandler(this.anchoAutomaticoToolStripMenuItem_Click);
            // 
            // todasLasCajasToolStripMenuItem
            // 
            this.todasLasCajasToolStripMenuItem.Name = "todasLasCajasToolStripMenuItem";
            this.todasLasCajasToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.todasLasCajasToolStripMenuItem.Text = "Mostrar cajas inactivas";
            this.todasLasCajasToolStripMenuItem.Click += new System.EventHandler(this.todasLasCajasToolStripMenuItem_Click);
            // 
            // limpiarHistorialToolStripMenuItem1
            // 
            this.limpiarHistorialToolStripMenuItem1.Name = "limpiarHistorialToolStripMenuItem1";
            this.limpiarHistorialToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.limpiarHistorialToolStripMenuItem1.Text = "Limpiar Historial";
            this.limpiarHistorialToolStripMenuItem1.Click += new System.EventHandler(this.limpiarHistorialToolStripMenuItem1_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 30);
            this.toolStripButton1.Text = "sync";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 33);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(336, 238);
            this.tabControl1.TabIndex = 6;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(328, 212);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "B. Almacen";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToOrderColumns = true;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Add3,
            this.Caja3,
            this.Viaje3,
            this.Fecha3,
            this.Almacen3,
            this.Cliente3,
            this.Buscar3,
            this.checkBox3,
            this.ID3});
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(3, 3);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(322, 206);
            this.dataGridView3.TabIndex = 8;
            this.dataGridView3.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
            // 
            // Add3
            // 
            this.Add3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Add3.FillWeight = 21F;
            this.Add3.HeaderText = "";
            this.Add3.Name = "Add3";
            this.Add3.ReadOnly = true;
            this.Add3.Text = "+";
            this.Add3.Width = 21;
            // 
            // Caja3
            // 
            this.Caja3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Caja3.DataPropertyName = "CAJA";
            this.Caja3.FillWeight = 21F;
            this.Caja3.HeaderText = "Caja";
            this.Caja3.Name = "Caja3";
            this.Caja3.ReadOnly = true;
            this.Caja3.Width = 53;
            // 
            // Viaje3
            // 
            this.Viaje3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Viaje3.DataPropertyName = "VIAJE";
            this.Viaje3.FillWeight = 21F;
            this.Viaje3.HeaderText = "Viaje";
            this.Viaje3.Name = "Viaje3";
            this.Viaje3.ReadOnly = true;
            this.Viaje3.Width = 55;
            // 
            // Fecha3
            // 
            this.Fecha3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Fecha3.DataPropertyName = "FECHA";
            this.Fecha3.FillWeight = 21F;
            this.Fecha3.HeaderText = "Fecha";
            this.Fecha3.Name = "Fecha3";
            this.Fecha3.ReadOnly = true;
            this.Fecha3.Width = 62;
            // 
            // Almacen3
            // 
            this.Almacen3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Almacen3.DataPropertyName = "ALMACEN";
            this.Almacen3.FillWeight = 21F;
            this.Almacen3.HeaderText = "Almacen";
            this.Almacen3.Name = "Almacen3";
            this.Almacen3.ReadOnly = true;
            this.Almacen3.Width = 73;
            // 
            // Cliente3
            // 
            this.Cliente3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Cliente3.DataPropertyName = "CLIENTE";
            this.Cliente3.FillWeight = 50F;
            this.Cliente3.HeaderText = "Cliente";
            this.Cliente3.Name = "Cliente3";
            this.Cliente3.ReadOnly = true;
            this.Cliente3.Width = 64;
            // 
            // Buscar3
            // 
            this.Buscar3.DataPropertyName = "BUSCAR";
            this.Buscar3.FillWeight = 21F;
            this.Buscar3.HeaderText = "";
            this.Buscar3.Name = "Buscar3";
            this.Buscar3.ReadOnly = true;
            this.Buscar3.Visible = false;
            this.Buscar3.Width = 21;
            // 
            // checkBox3
            // 
            this.checkBox3.DataPropertyName = "CHECK";
            this.checkBox3.FillWeight = 21F;
            this.checkBox3.HeaderText = "Check";
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.ReadOnly = true;
            this.checkBox3.Visible = false;
            this.checkBox3.Width = 21;
            // 
            // ID3
            // 
            this.ID3.DataPropertyName = "ID";
            this.ID3.HeaderText = "ID";
            this.ID3.Name = "ID3";
            this.ID3.ReadOnly = true;
            this.ID3.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(328, 212);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Busqueda";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Add2,
            this.Caja2,
            this.Viaje2,
            this.Fecha2,
            this.Almacen2,
            this.Cliente2,
            this.Buscar2,
            this.checkBox2,
            this.ID2});
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(322, 206);
            this.dataGridView2.TabIndex = 7;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            this.dataGridView2.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
            // 
            // Add2
            // 
            this.Add2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Add2.FillWeight = 21F;
            this.Add2.HeaderText = "";
            this.Add2.Name = "Add2";
            this.Add2.ReadOnly = true;
            this.Add2.Text = "+";
            this.Add2.Width = 21;
            // 
            // Caja2
            // 
            this.Caja2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Caja2.DataPropertyName = "CAJA";
            this.Caja2.FillWeight = 21F;
            this.Caja2.HeaderText = "Caja";
            this.Caja2.Name = "Caja2";
            this.Caja2.ReadOnly = true;
            this.Caja2.Width = 53;
            // 
            // Viaje2
            // 
            this.Viaje2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Viaje2.DataPropertyName = "VIAJE";
            this.Viaje2.FillWeight = 21F;
            this.Viaje2.HeaderText = "Viaje";
            this.Viaje2.Name = "Viaje2";
            this.Viaje2.ReadOnly = true;
            this.Viaje2.Width = 55;
            // 
            // Fecha2
            // 
            this.Fecha2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Fecha2.DataPropertyName = "FECHA";
            this.Fecha2.FillWeight = 21F;
            this.Fecha2.HeaderText = "Fecha";
            this.Fecha2.Name = "Fecha2";
            this.Fecha2.ReadOnly = true;
            this.Fecha2.Width = 62;
            // 
            // Almacen2
            // 
            this.Almacen2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Almacen2.DataPropertyName = "ALMACEN";
            this.Almacen2.FillWeight = 21F;
            this.Almacen2.HeaderText = "Almacen";
            this.Almacen2.Name = "Almacen2";
            this.Almacen2.ReadOnly = true;
            this.Almacen2.Width = 73;
            // 
            // Cliente2
            // 
            this.Cliente2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Cliente2.DataPropertyName = "CLIENTE";
            this.Cliente2.FillWeight = 50F;
            this.Cliente2.HeaderText = "Cliente";
            this.Cliente2.Name = "Cliente2";
            this.Cliente2.ReadOnly = true;
            this.Cliente2.Width = 64;
            // 
            // Buscar2
            // 
            this.Buscar2.DataPropertyName = "BUSCAR";
            this.Buscar2.FillWeight = 21F;
            this.Buscar2.HeaderText = "";
            this.Buscar2.Name = "Buscar2";
            this.Buscar2.ReadOnly = true;
            this.Buscar2.Visible = false;
            this.Buscar2.Width = 21;
            // 
            // checkBox2
            // 
            this.checkBox2.DataPropertyName = "CHECK";
            this.checkBox2.FillWeight = 21F;
            this.checkBox2.HeaderText = "Check";
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.ReadOnly = true;
            this.checkBox2.Visible = false;
            this.checkBox2.Width = 21;
            // 
            // ID2
            // 
            this.ID2.DataPropertyName = "ID";
            this.ID2.HeaderText = "ID";
            this.ID2.Name = "ID2";
            this.ID2.ReadOnly = true;
            this.ID2.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(328, 212);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "Cajas Pendientes";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckBox,
            this.Caja,
            this.Viaje,
            this.Fecha,
            this.Almacen,
            this.Cliente,
            this.Buscar,
            this.id});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(322, 206);
            this.dataGridView1.TabIndex = 6;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
            // 
            // CheckBox
            // 
            this.CheckBox.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CheckBox.DataPropertyName = "CHECK";
            this.CheckBox.FillWeight = 21F;
            this.CheckBox.Frozen = true;
            this.CheckBox.HeaderText = "";
            this.CheckBox.Name = "CheckBox";
            this.CheckBox.Width = 21;
            // 
            // Caja
            // 
            this.Caja.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Caja.DataPropertyName = "CAJA";
            this.Caja.FillWeight = 21F;
            this.Caja.Frozen = true;
            this.Caja.HeaderText = "Caja";
            this.Caja.Name = "Caja";
            this.Caja.Width = 53;
            // 
            // Viaje
            // 
            this.Viaje.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Viaje.DataPropertyName = "VIAJE";
            this.Viaje.FillWeight = 21F;
            this.Viaje.Frozen = true;
            this.Viaje.HeaderText = "Viaje";
            this.Viaje.Name = "Viaje";
            this.Viaje.Width = 55;
            // 
            // Fecha
            // 
            this.Fecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Fecha.DataPropertyName = "FECHA";
            this.Fecha.FillWeight = 21F;
            this.Fecha.HeaderText = "Fecha";
            this.Fecha.Name = "Fecha";
            this.Fecha.Width = 62;
            // 
            // Almacen
            // 
            this.Almacen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Almacen.DataPropertyName = "ALMACEN";
            this.Almacen.FillWeight = 21F;
            this.Almacen.HeaderText = "Almacen";
            this.Almacen.Name = "Almacen";
            this.Almacen.Width = 73;
            // 
            // Cliente
            // 
            this.Cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Cliente.DataPropertyName = "CLIENTE";
            this.Cliente.FillWeight = 50F;
            this.Cliente.HeaderText = "Cliente";
            this.Cliente.Name = "Cliente";
            this.Cliente.Width = 64;
            // 
            // Buscar
            // 
            this.Buscar.DataPropertyName = "BUSCAR";
            this.Buscar.FillWeight = 21F;
            this.Buscar.HeaderText = "";
            this.Buscar.Name = "Buscar";
            this.Buscar.Visible = false;
            this.Buscar.Width = 21;
            // 
            // id
            // 
            this.id.DataPropertyName = "ID";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.LinkColor = System.Drawing.Color.Red;
            this.linkLabel1.Location = new System.Drawing.Point(237, 236);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Padding = new System.Windows.Forms.Padding(5);
            this.linkLabel1.Size = new System.Drawing.Size(91, 27);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Deshacer 3";
            this.linkLabel1.Visible = false;
            this.linkLabel1.VisibleChanged += new System.EventHandler(this.linkLabel1_VisibleChanged);
            this.linkLabel1.Click += new System.EventHandler(this.linkLabel1_Click);
            // 
            // configuracionesPorDefectoToolStripMenuItem
            // 
            this.configuracionesPorDefectoToolStripMenuItem.Name = "configuracionesPorDefectoToolStripMenuItem";
            this.configuracionesPorDefectoToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.configuracionesPorDefectoToolStripMenuItem.Text = "Configuraciones por defecto";
            this.configuracionesPorDefectoToolStripMenuItem.Click += new System.EventHandler(this.configuracionesPorDefectoToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 271);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(310, 310);
            this.Name = "Form1";
            this.Text = "Ubicacion de cajas";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripTextBox tbxBuscar;
        private System.Windows.Forms.ToolStripButton btnBuscar;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem anchoAutomaticoToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.ToolStripMenuItem todasLasCajasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem limpiarHistorialToolStripMenuItem1;
        private System.Windows.Forms.DataGridViewButtonColumn Add3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Caja3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Viaje3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Almacen3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cliente3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buscar3;
        private System.Windows.Forms.DataGridViewTextBoxColumn checkBox3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID3;
        private System.Windows.Forms.DataGridViewButtonColumn Add2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Caja2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Viaje2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Almacen2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cliente2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buscar2;
        private System.Windows.Forms.DataGridViewTextBoxColumn checkBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn Caja;
        private System.Windows.Forms.DataGridViewTextBoxColumn Viaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Almacen;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buscar;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripMenuItem configuracionesPorDefectoToolStripMenuItem;
    }
}

