﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UbicacionDeCajas
{
    public partial class Form1 : Form
    {
        bool Ord = false;
        string Col = "Viaje";
        Class.Connect con = new Class.Connect();
        List<Class.Connect> listaTrayectos = new List<Class.Connect>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage2;
            tabControl1.SelectedTab = tabPage1;

            anchoAutomaticoToolStripMenuItem.Checked = Properties.Settings.Default.AnchoAuto;
            todasLasCajasToolStripMenuItem.Checked = Properties.Settings.Default.CajasInactivas;

            if (!Properties.Settings.Default.AnchoAuto)
            {
                this.Width = Properties.Settings.Default.Ancho;
                this.Height = Properties.Settings.Default.Alto;
            }
            else
                this.Height = Properties.Settings.Default.Alto;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Text == "Cajas Pendientes")
            {
                string T = tbxBuscar.Text;
                tbxBuscar.Text = "";
                BuscarCajas();
                tbxBuscar.Text = T;
            }
        }

        #region //-------------------------------  METODOS  -------------------------------// 
        int RowVisible = 0;

        private void GridTamano()
        {
            if (anchoAutomaticoToolStripMenuItem.Checked)
            {
                int total = 30;

                int totalD1 =
                    dataGridView1.Columns["CheckBox"].Width +
                    dataGridView1.Columns["Caja"].Width +
                    dataGridView1.Columns["Viaje"].Width +
                    dataGridView1.Columns["Almacen"].Width +
                    dataGridView1.Columns["Cliente"].Width +
                    dataGridView1.Columns["Fecha"].Width;

                int totalD2 = dataGridView2.Columns["Add2"].Width +
                    dataGridView2.Columns["Caja2"].Width +
                    dataGridView2.Columns["Viaje2"].Width +
                    dataGridView2.Columns["Almacen2"].Width +
                    dataGridView2.Columns["Cliente2"].Width +
                    dataGridView2.Columns["Fecha2"].Width;

                int totalD3 = dataGridView3.Columns["Add3"].Width +
                    dataGridView3.Columns["Caja3"].Width +
                    dataGridView3.Columns["Viaje3"].Width +
                    dataGridView3.Columns["Almacen3"].Width +
                    dataGridView3.Columns["Cliente3"].Width +
                    dataGridView3.Columns["Fecha3"].Width;

                bool dG1 = this.IsScrollBarVisible(this.dataGridView1);
                bool dG2 = this.IsScrollBarVisible(this.dataGridView2);
                bool dG3 = this.IsScrollBarVisible(this.dataGridView3);

                if (totalD1 < totalD2)
                {
                    if (totalD2 < totalD3)
                        total += totalD3;
                    else
                        total += totalD2;
                }
                else if (totalD1 > totalD2)
                {
                    if (totalD1 < totalD3)
                        total += totalD3;
                    else
                        total += totalD1;
                }

                if (dG1 || dG2 || dG3)
                    total += 20;

                if (this.Width < total)
                    this.Width = total;
            }
        }

        private List<Class.Connect> Organiza(List<Class.Connect> Trayectos, string Organiza, bool Orden)
        {
            if (Orden)
            {
                if (Organiza == "Caja")
                    Trayectos.Sort((x, y) => x.Caja.CompareTo(y.Caja));
                if (Organiza == "Viaje")
                    Trayectos.Sort((x, y) => x.Viaje.CompareTo(y.Viaje));
                if (Organiza == "Fecha")
                    Trayectos.Sort((x, y) => x.Fecha.CompareTo(y.Fecha));
                if (Organiza == "Cliente")
                    Trayectos.Sort((x, y) => x.Cliente.CompareTo(y.Cliente));
                if (Organiza == "Almacen")
                    Trayectos.Sort((x, y) => x.Almacen.CompareTo(y.Almacen));
            }
            else //desc
            {
                if (Organiza == "Caja")
                    Trayectos.Sort((x, y) => y.Caja.CompareTo(x.Caja));
                if (Organiza == "Viaje")
                    Trayectos.Sort((x, y) => y.Viaje.CompareTo(x.Viaje));
                if (Organiza == "Fecha")
                    Trayectos.Sort((x, y) => y.Fecha.CompareTo(x.Fecha));
                if (Organiza == "Cliente")
                    Trayectos.Sort((x, y) => y.Cliente.CompareTo(x.Cliente));
                if (Organiza == "Almacen")
                    Trayectos.Sort((x, y) => y.Almacen.CompareTo(x.Almacen));
            }
            return listaTrayectos;
        }

        private void BuscarCajas()
        {
            con.Buscar = tbxBuscar.Text;
            tabControl1.SelectedTab = tabPage1;

            if (!todasLasCajasToolStripMenuItem.Checked)
                listaTrayectos = con.GetAllCajas();
            else
                listaTrayectos = con.GetCajasPendientes();

            dataGridView1.DataSource = Organiza(listaTrayectos, Col, Ord);
            GridTamano();
        }

        private void BuscarViajes()
        {
            con.Buscar = tbxBuscar.Text;
            tabControl1.SelectedTab = tabPage2;
            listaTrayectos = con.GetAllTrayecto();
            dataGridView2.DataSource = Organiza(listaTrayectos, Col, Ord);
            GridTamano();
        }

        private void BuscarAlmacen()
        {
            con.Buscar = tbxBuscar.Text;
            tabControl1.SelectedTab = tabPage3;
            listaTrayectos = con.GetAllAlmacen();
            dataGridView3.DataSource = Organiza(listaTrayectos, Col, Ord);
            GridTamano();
        }

        private bool IsScrollBarVisible(Control aControl)
        {
            foreach (Control c in aControl.Controls)
            {
                if (c.GetType().Equals(typeof(VScrollBar)))
                {
                    return c.Visible;
                }
            }
            return false;
        }
        #endregion        

        #region //-----------------------------  BUSCAR CLICK  ----------------------------// 
        private void tbxBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = char.ToUpper(e.KeyChar);

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (tabControl1.SelectedTab == tabPage1)
                    BuscarCajas();
                else if (tabControl1.SelectedTab == tabPage2)
                    BuscarViajes();
                else if (tabControl1.SelectedTab == tabPage3)
                    BuscarAlmacen();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabPage1)
                BuscarCajas();
            else if (tabControl1.SelectedTab == tabPage2)
                BuscarViajes();
            else if (tabControl1.SelectedTab == tabPage3)
                BuscarAlmacen();
        }
        #endregion

        #region //------------------------  CLICK EN DATAGRIDVIEW  ------------------------// 
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn && e.RowIndex >= 0)
            {
                try
                {
                    RowVisible = e.RowIndex;

                    linkLabel1.Visible = true;

                    dataGridView1.CurrentCell = null;
                    dataGridView1.Rows[e.RowIndex].Visible = false;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                try
                {
                    con.Check = false;
                    con.Cliente = dataGridView2.Rows[dataGridView2.CurrentCell.RowIndex].Cells["Cliente2"].Value.ToString();
                    con.Caja = dataGridView2.Rows[dataGridView2.CurrentCell.RowIndex].Cells["Caja2"].Value.ToString();
                    con.Viaje = Convert.ToInt32(dataGridView2.Rows[dataGridView2.CurrentCell.RowIndex].Cells["Viaje2"].Value);
                    con.Almacen = dataGridView2.Rows[dataGridView2.CurrentCell.RowIndex].Cells["Almacen2"].Value.ToString();
                    con.Fecha = dataGridView2.Rows[dataGridView2.CurrentCell.RowIndex].Cells["Fecha2"].Value.ToString();
                    if (con.Fecha == "Sin Fecha")
                        con.Fecha = DateTime.Now.ToString();

                    con.InsertarCaja();

                    tabControl1.SelectedTab = tabPage1;
                }
                catch (Exception)
                {
                    //throw;
                }
            }
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridView DG = (DataGridView)sender;

            if (Col == DG.Columns[e.ColumnIndex].HeaderText)
                Ord = !Ord;
            else if (e.ColumnIndex != 0)
            {
                Col = DG.Columns[e.ColumnIndex].HeaderText;
                Ord = true;
            }

            DG.DataSource = Organiza(listaTrayectos, Col, Ord);

            DG.Refresh();
        }
        #endregion

        #region //----------------------------  LINK DESHACER  ----------------------------//       

        private async void linkLabel1_VisibleChanged(object sender, EventArgs e)
        {
            //------  SI ESTA VISIBLE MUESTRA TEMPORALIZADOR  ------//
            if (linkLabel1.Visible == true)
            {
                CheckForIllegalCrossThreadCalls = false;

                Task oTask = new Task(Deshacer);

                oTask.Start();

                await oTask;
            }
            else if (linkLabel1.Visible == false)
            {
                //--  SI NO SE ARREPIENTE ACTUALIZA EN BASE DE DATOS  --//
                if (Convert.ToBoolean(dataGridView1.Rows[RowVisible].Cells["CheckBox"].Value))
                {
                    //CheckForIllegalCrossThreadCalls = true;                
                    con.Check = Convert.ToBoolean(dataGridView1.Rows[RowVisible].Cells["CheckBox"].Value);
                    con.Id = dataGridView1.Rows[RowVisible].Cells["id"].Value.ToString();
                }
                con.UpdateCaja();
            }
        }

        //---------------  CANCELAR ANTES DE 0  ---------------//
        private void linkLabel1_Click(object sender, EventArgs e)
        {
            //------  HACE LA FILA VISIBLE Y QUITA EL CHECK  ------//
            dataGridView1.Rows[RowVisible].Cells["CheckBox"].Value = false;
            dataGridView1.Rows[RowVisible].Visible = true;
            linkLabel1.Visible = false;
        }

        //-------------  TEMPORIZADOR 3 SEGUNDOS  -------------//
        public void Deshacer()
        {
            for (int i = 0; i < 4; i++)
            {
                //------------  CAMBIA TEXTO CADA SEGUNDO  ------------//
                linkLabel1.Text = "Deshacer " + (3 - i).ToString();
                Thread.Sleep(1000);
            }

            linkLabel1.Visible = false;
        }
        #endregion

        #region //-----------------------  MENUSTRIP CONFIGUTACION  -----------------------// 

        private void anchoAutomaticoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //-----------------  ANCHO AUTOMATICO  -----------------//
            Properties.Settings.Default.AnchoAuto = !Properties.Settings.Default.AnchoAuto;
            Properties.Settings.Default.Save();

            anchoAutomaticoToolStripMenuItem.Checked = Properties.Settings.Default.AnchoAuto;
        }

        private void todasLasCajasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //--  SELECCIONA TODAS LAS CAJAS ACTIVAS O INACTIVAS  --//
            Properties.Settings.Default.CajasInactivas = !Properties.Settings.Default.CajasInactivas;
            Properties.Settings.Default.Save();

            todasLasCajasToolStripMenuItem.Checked = Properties.Settings.Default.CajasInactivas;

            tabControl1.SelectedTab = tabPage1;
        }

        private void limpiarHistorialToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //---------  BORRAR TODAS LAS CAJAS INACTIVAS  ---------//
            if (MessageBox.Show("Desea ELIMINAR el historial de cajas pendientes", "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                con.DeleteCaja();

                tabControl1.SelectedTab = tabPage1;
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            con.Actualizar();

            string T = tbxBuscar.Text;
            tbxBuscar.Text = "";
            BuscarCajas();
            tbxBuscar.Text = T;
        }

        private void configuracionesPorDefectoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //-----------------  DEFAULT SETTINGS  -----------------//
            if (MessageBox.Show("Desea RESTABLECER las configuraciones por defecto?", "Restablrcer", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Properties.Settings.Default.Ancho = 310;
                Properties.Settings.Default.Alto = 310;
                Properties.Settings.Default.AnchoAuto = true;
                Properties.Settings.Default.CajasInactivas = false;

                Properties.Settings.Default.Save();

                configuracionesPorDefectoToolStripMenuItem.Checked = true;

                Application.Restart();
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!Properties.Settings.Default.AnchoAuto)
            {
                Properties.Settings.Default.Ancho = this.Width;
                Properties.Settings.Default.Alto = this.Height;
                Properties.Settings.Default.Save();
            }
            else if (configuracionesPorDefectoToolStripMenuItem.Checked != true)
            {
                Properties.Settings.Default.Alto = this.Height;
                Properties.Settings.Default.Save();
            }
        }
    }
}
